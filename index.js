import remove from 'unist-util-remove'
import visit from 'unist-util-visit'

const NAME = 'footnote-reference-definition'

function idMsg (node) {
  return `id "${node.identifier}"`
}

export default (opts) => {
  return (root, f) => {
    const refs = {}
    visit(root, 'footnote', (node, index, parent) => {
      node.children = [{
        type: 'paragraph',
        children: node.children,
        position: node.position
      }]
    })
    visit(root, 'footnoteReference', (node, index, parent) => {
      refs[node.identifier] = node
    })
    visit(root, 'footnoteDefinition', (node, index, parent) => {
      const ref = refs[node.identifier]
      const im = idMsg(node)
      if (!ref) {
        f.message(
          `footnote reference does not exist for definition with ${im}`,
          node.position, `${NAME}:reference-exists`)
        return
      }
      if (ref.definition) {
        f.message(`footnote definition already exists for ${im}`,
          node.position, `${NAME}:no-repeat-definition`)
        return
      }
      ref.type = 'footnote'
      ref.children = node.children
    })
    Object.values(refs)
      .filter(n => !!n.definition)
      .forEach(n => {
        f.message(`footnote reference never defined with ${idMsg(n)}`,
          n.position, `${NAME}:reference-defined`)
      })
    remove(root, { cascade: true }, (n) => n.type === 'footnoteReference')
    remove(root, { cascade: true }, (n) => n.type === 'footnoteDefinition')
  }
}
